﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour 
{
	public GameObject[] enemyPrefabs;

	public float startingSpawnInterval = 2f;
	public float spawnInterval = 2.5f;
	private float currentCountdown;

	private void Awake()
	{
		currentCountdown = startingSpawnInterval;
	}

	private void Update()
	{
		currentCountdown -= Time.deltaTime;
		if (currentCountdown <= 0f && GameMaster.instance.playerDisplayPairs.Count > 0)
		{
			SpawnEnemy();
			currentCountdown = spawnInterval;
		}
	}

	private void SpawnEnemy()
	{
		spawnInterval *= 0.98f;

		int maxRndIndex =
			(Chronometer.time >= 20f) ? 4 :
			(Chronometer.time >= 15f) ? 3 :
			(Chronometer.time >= 8f) ? 2 :
			1;

		int rndIndex = Random.Range(0, maxRndIndex);

		Vector3 position = Vector3.zero;
		position.z = -Camera.main.transform.position.z;
		position.x = Random.Range(0.1f, 0.9f);
		position.y = Random.Range(0.1f, 0.9f);

		GameObject newEnemyGameObject = (GameObject) Instantiate(enemyPrefabs[rndIndex], Camera.main.ViewportToWorldPoint(position), Quaternion.identity);
		newEnemyGameObject.transform.SetParent(transform);

		AIController enemyController = newEnemyGameObject.GetComponent<AIController>();
		enemyController.currentTarget = GameMaster.GetRandomPlayer().transform;
	}
}
