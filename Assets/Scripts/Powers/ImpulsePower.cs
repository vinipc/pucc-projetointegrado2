﻿using UnityEngine;
using System.Collections;

public class ImpulsePower : Power
{
	public int blinks;

	public override void ActivatePower(Player player)
	{
		player.StartCoroutine(ActivateImpulse(player));
	}

	private IEnumerator ActivateImpulse(Player player)
	{
		player.isTurboed = true;
		player.isImmune = true;

		for (int i = 0; i < blinks; i++)
		{
			player.speedSprite.enabled = false;
			yield return new WaitForSeconds(1.5f / (float) blinks);
			player.speedSprite.enabled = true;
			yield return new WaitForSeconds(1.5f / (float) blinks);
		}

		//yield return new WaitForSeconds(3f);

		player.isTurboed = false;
		player.isImmune = false;

		DestroyPower();
		player.OnPowerDestroyed();
	}
}