﻿using UnityEngine;
using System.Collections;

public class ShellPower : Power 
{
	public GameObject shellPrefab;

	public override void ActivatePower(Player player)
	{
		Vector3 direction = (Vector3) player.GetComponent<Controller>().direction;

		Vector3 spawnPosition = player.transform.position - direction * 1.2f;
		GameObject spawnedSpike = (GameObject) Instantiate(shellPrefab, spawnPosition, player.transform.rotation);

		spawnedSpike.GetComponent<Shell>().ThrowShell(-direction);
		player.OnPowerDestroyed();
		DestroyPower();
	}
}