﻿using UnityEngine;
using System.Collections;

public class Player : Character 
{
	public PlayerLivesDisplay livesDisplay;

	public SpriteRenderer defaultSprite;
	public SpriteRenderer spikeSprite;
	public SpriteRenderer freezeSprite;
	public SpriteRenderer shellSprite;
	public SpriteRenderer speedSprite;
	public GameObject iceRing;

	public Power currentPower;

	protected override void Awake()
	{
		base.Awake();
		MultiplayerCamera.AddPlayer(this);

		spikeSprite.enabled = false;
		Power power = GetComponentInChildren<Power>(true);
		if (power)
		{
			PickUpPower(power);
		}
	}

	protected override void UpdateLivesDisplay()
	{
		livesDisplay.UpdateDisplay(lives.Count);
	}

	protected override void Die()
	{
		MultiplayerCamera.RemovePlayer(this);
		GameMaster.RemovePlayer(this);
		base.Die();
	}

	public void PickUpPower(Power newPower)
	{
		if (currentPower != null)
		{
			return;
		}
		
		newPower.AttachToCharacter(transform);
		currentPower = newPower;

		defaultSprite.enabled = false;
		spikeSprite.enabled = newPower is SpikePower;
		freezeSprite.enabled = newPower is FreezePower;
		shellSprite.enabled = newPower is ShellPower;
		speedSprite.enabled = newPower is ImpulsePower;

		iceRing.SetActive(newPower is FreezePower);
	}

	public override void HitByBasicPower(GameObject enemy)
	{
		Vector2 direction = (enemy.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 250f);
		DecreaseLives(1);
	}

	public override void HitBySpikedEnemy(GameObject enemy)
	{
		Vector2 direction = (enemy.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 250f);
		DecreaseLives(2);
	}

	protected override void DecreaseLives(int numberOfLives)
	{
		base.DecreaseLives(numberOfLives);
		AudioMaster.PlayerHurt();
	}

	public void DropPower()
	{
		currentPower.Unattach();
		currentPower = null;
	}

	public void OnPowerDestroyed()
	{
		defaultSprite.enabled = true;
		spikeSprite.enabled = false;
		freezeSprite.enabled = false;
		shellSprite.enabled = false;
		speedSprite.enabled = false;
		iceRing.SetActive(false);
	}

	public void Fire()
	{
		if (currentPower != null)
		{
			currentPower.ActivatePower(this);
		}
	}
}