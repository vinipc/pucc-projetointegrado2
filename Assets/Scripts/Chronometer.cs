﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Chronometer : MonoBehaviour 
{
	public static float time;
	public static bool count = true;

	private Text display;

	private void Start()
	{
		display = GetComponent<Text>();
		time = 0f;
	}

	private void Update()
	{
		if (count)
		{
			time += Time.deltaTime;
			display.text = time.ToString("0.0");
		}
	}
}