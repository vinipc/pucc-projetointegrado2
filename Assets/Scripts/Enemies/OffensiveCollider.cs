﻿using UnityEngine;
using System.Collections;

public class OffensiveCollider : MonoBehaviour 
{
	new private Collider2D collider;
	private Enemy parentEnemy;

	private void Awake()
	{
		parentEnemy = transform.GetComponentInParent<Enemy>();
		collider = GetComponent<Collider2D>();
	}

	private void OnTriggerEnter2D(Collider2D otherCollider)
	{
		parentEnemy.OffensiveTriggerEntered(otherCollider, collider);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		parentEnemy.OffensiveColliderHit(collision, collider);
	}
}