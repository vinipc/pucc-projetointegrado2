﻿using UnityEngine;
using System.Collections;

public class PlayerController : Controller 
{
	public enum PlayerControl {P1, P2, P3, P4};
	public PlayerControl playerControl;
	private Player player;

	protected override void Awake()
	{
		player = GetComponent<Player>();
		base.Awake();
	}

	protected override void Update()
	{
		base.Update();
		if (Input.GetButtonDown("Fire" + playerControl.ToString()))
		{
			player.Fire();
		}
	}

	protected void FixedUpdate() 
	{
		Vector2 input = new Vector2(Input.GetAxis("Horizontal" + playerControl.ToString()), Input.GetAxis("Vertical" + playerControl.ToString()));
		if (input.sqrMagnitude > 0f)
		{
			rigidbody.AddForce(input.normalized * currentMoveForce);
			direction = input.normalized;
		}
	}
}