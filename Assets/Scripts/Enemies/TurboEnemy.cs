﻿using UnityEngine;
using System.Collections;

public class TurboEnemy : Enemy 
{
	public void OnCollisionEnter2D(Collision2D coll)
	{
		if (rigidbody.velocity.sqrMagnitude >= Mathf.Pow(defaultMaxVelocity, 2f))
		{
			Character hitCharacter = coll.gameObject.GetComponent<Character>();
			if (hitCharacter)
				hitCharacter.HitByTurbo(gameObject);
		}
	}
}
