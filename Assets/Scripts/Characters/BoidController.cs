﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoidController : AIController 
{
	public const float MATCH_VELOCITY_OWN_WEIGHT = 2f;

	public BoidsManager manager;

	protected override void Update()
	{
		maxVelocity = manager.boidProperties.maxVelocity;
		maxMoveForce = manager.boidProperties.moveForce;

		base.Update();
	}

	protected void LateUpdate()
	{
		MoveBoid(manager.boids);
	}

	private void MoveBoid(List<BoidController> boids)
	{
		Vector2 awayMovement = MoveAway(boids) * manager.boidProperties.spreadWeight;
		Vector2 matchMovement = MoveToMatch(boids) * manager.boidProperties.matchVelocityWeight;
		Vector2 centerMovement = MoveToCenter(boids) * manager.boidProperties.togethernessWeight;
		Vector2 mouseMovement = MoveTowardsTarget(boids) * manager.boidProperties.targetWeight;

		Vector2 deltaVelocity = (awayMovement + matchMovement + centerMovement + mouseMovement).normalized;
		deltaVelocity *= maxMoveForce;
		deltaVelocity *= Time.deltaTime;

		rigidbody.AddForce(deltaVelocity);
	}

	private Vector2 MoveToCenter(List<BoidController> boids)
	{
		Vector2 averagePosition = Vector2.zero;

		for (int i = 0; i < boids.Count; i++)
		{
			if (boids[i] != this)
			{
				averagePosition += (Vector2) boids[i].transform.position;
			}
		}

		averagePosition /= boids.Count - 1;

		return averagePosition - (Vector2) transform.position;
	}

	private Vector2 MoveAway(List<BoidController> boids)
	{
		Vector2 movement = Vector2.zero;

		for (int i = 0; i < boids.Count; i++)
		{
			if (boids[i] != this)
			{
				if ((transform.position - boids[i].transform.position).sqrMagnitude < manager.boidProperties.maxDistance)
				{
					movement -= (Vector2) (boids[i].transform.position - transform.position);
				}
			}
		}

		return movement;
	}

	private Vector2 MoveToMatch(List<BoidController> boids)
	{
		Vector2 averageVelocity = Vector2.zero;

		for (int i = 0; i < boids.Count; i++)
		{
			if (boids[i] != this)
			{
				averageVelocity += boids[i].rigidbody.velocity;
			}
		}

		averageVelocity /= boids.Count - 1;
		return (averageVelocity - rigidbody.velocity) * MATCH_VELOCITY_OWN_WEIGHT;
	}

	private Vector2 MoveTowardsTarget(List<BoidController> boids)
	{
		Vector2 targetPosition;
		if (currentTarget)
		{
			targetPosition = currentTarget.transform.position;
		}
		else
		{
			targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		}
		
		Vector2 movement = targetPosition - (Vector2) transform.position;

		return movement;
	}
}