﻿using UnityEngine;
using System.Collections;

public class WatchArea : MonoBehaviour 
{
	private AIController controller;

	private void Awake()
	{
		controller = GetComponentInParent<AIController>();
	}

	private void OnTriggerEnter2D(Collider2D otherColl)
	{
		controller.ObjectEnteredWatchArea(otherColl.transform);
	}
}
