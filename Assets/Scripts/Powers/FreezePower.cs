﻿using UnityEngine;
using System.Collections;

public class FreezePower : Power 
{
	public GameObject freezePrefab;

	public override void ActivatePower(Player player)
	{
		GameObject freeze = (GameObject) Instantiate(freezePrefab, transform.position, Quaternion.identity);
		freeze.transform.SetParent(player.transform, false);
		freeze.transform.localPosition = player.iceRing.transform.localPosition;

		DestroyPower();
	}
}