﻿using UnityEngine;
using System.Collections;

public class Spike : MonoBehaviour 
{
	new public Rigidbody2D rigidbody;
	public float force;
	public float torque;
	public float lifetime = 3f;

	public void OnTriggerEnter2D(Collider2D otherColl)
	{
		Enemy hitEnemy = otherColl.GetComponent<Enemy>();

		if (hitEnemy)
		{
			hitEnemy.HitByPlayerSpike(this);
		}
	}

	public void ThrowSpike(Vector3 direction)
	{
		StartCoroutine(ThrowSpikeCoroutine(direction));
	}

	private IEnumerator ThrowSpikeCoroutine(Vector3 direction)
	{
		rigidbody.AddForce(direction * force, ForceMode2D.Impulse);
		rigidbody.AddTorque(torque);

		yield return new WaitForSeconds(lifetime);

		Destroy(gameObject);
	}
}