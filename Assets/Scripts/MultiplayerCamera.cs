﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultiplayerCamera : MonoBehaviour 
{
	private static List<Player> players = new List<Player>();

	public Vector2 fovRange = new Vector2(8f, 8f);

	private void LateUpdate()
	{
		Vector3 newPosition = GetCenterPoint();
		newPosition.z = transform.position.z;
		transform.position = newPosition;
	}

	public static void AddPlayer(Player newPlayer)
	{
		if (!players.Contains(newPlayer))
			players.Add(newPlayer);
	}

	public static void RemovePlayer(Player oldPlayer)
	{
		players.Remove(oldPlayer);
	}

	private Vector3 GetCenterPoint()
	{
		Vector3 center = Vector3.zero;

		if (players.Count > 0)
		{
			for (int i = 0; i < players.Count; i++)
			{
				center += players[i].transform.position;
			}
			center /= players.Count;
		}

		return center;
	}
}