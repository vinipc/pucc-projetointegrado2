﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct BoidProperties
{	
	public float maxVelocity;
	public float moveForce;
	[Range(0f, 10f)] public float maxDistance;

	[Range(0f, 1f)] public float spreadWeight;
	[Range(0f, 1f)] public float togethernessWeight;
	[Range(0f, 1f)] public float matchVelocityWeight;
	[Range(0f, 1f)] public float targetWeight;
}

public class BoidsManager : MonoBehaviour 
{
	public GameObject boidPrefab;
	[HideInInspector] public List<BoidController> boids = new List<BoidController>();
	public int numberOfBoids;
	public BoidProperties boidProperties;

	private int numberOfSavedValues = 0;
	public List<BoidProperties> savedProperties;

	private void Awake()
	{
		Randomize();

		for (int i = 0; i < numberOfBoids; i++)
		{
			CreateBoid();
		}

		if (PlayerPrefs.HasKey("NumberOfSavedValues"))
		{
			numberOfSavedValues = PlayerPrefs.GetInt("NumberOfSavedValues");
		}

		LoadValues();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			Randomize();
		}

		if (numberOfBoids > boids.Count)
		{
			CreateBoid();
		}
		else if (numberOfBoids < boids.Count)
		{
			Destroy(boids[0].gameObject);
			boids.RemoveAt(0);
		}
	}

	private void CreateBoid()
	{
		Vector2 boidPos = new Vector2();
		boidPos.x = Random.value;
		boidPos.y = Random.value;
		BoidController newBoid = Instantiate<GameObject>(boidPrefab).GetComponent<BoidController>();
		boids.Add(newBoid);
		newBoid.manager = this;
		newBoid.transform.SetParent(transform);
		newBoid.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(boidPos.x, boidPos.y, -Camera.main.transform.position.z));
	}

	private void Randomize()
	{
		boidProperties.maxVelocity = Random.Range(0f, 10f);
		boidProperties.moveForce = Random.Range(10f, 1000f);
		boidProperties.maxDistance = Random.Range(0f, 10f);

		boidProperties.spreadWeight = Random.Range(0f, 1f);
		boidProperties.togethernessWeight = Random.Range(0f, 1f);
		boidProperties.matchVelocityWeight = Random.Range(0f, 1f);
		boidProperties.targetWeight = Random.Range(0f, 1f);
	}

	[ContextMenu("Load values")]
	private void LoadValues()
	{
		numberOfSavedValues = PlayerPrefs.GetInt("NumberOfSavedValues", 0);
		savedProperties = new List<BoidProperties>();

		for (int i = 0; i < numberOfSavedValues; i++)
		{
			BoidProperties newProperties = new BoidProperties();
			newProperties.maxVelocity = PlayerPrefs.GetFloat("MaxVelocity_" + i);
			newProperties.moveForce = PlayerPrefs.GetFloat("MoveForce" + i);
			newProperties.maxDistance = PlayerPrefs.GetFloat("MaxDistance_" + i);

			newProperties.spreadWeight = PlayerPrefs.GetFloat("SpreadWeight_" + i);
			newProperties.togethernessWeight = PlayerPrefs.GetFloat("TogethernessWeight_" + i);
			newProperties.matchVelocityWeight = PlayerPrefs.GetFloat("MatchVelocityWeight_" + i);
			newProperties.targetWeight = PlayerPrefs.GetFloat("TargetWeight_" + i);

			savedProperties.Add(newProperties);
		}
	}

	[ContextMenu("Save values")]
	private void SaveValues()
	{
		PlayerPrefs.SetFloat("MaxVelocity_" + numberOfSavedValues, boidProperties.maxVelocity);
		PlayerPrefs.SetFloat("MoveForce" + numberOfSavedValues, boidProperties.moveForce);
		PlayerPrefs.SetFloat("MaxDistance_" + numberOfSavedValues, boidProperties.maxDistance);

		PlayerPrefs.SetFloat("SpreadWeight_" + numberOfSavedValues, boidProperties.spreadWeight);
		PlayerPrefs.SetFloat("TogethernessWeight_" + numberOfSavedValues, boidProperties.togethernessWeight);
		PlayerPrefs.SetFloat("MatchVelocityWeight_" + numberOfSavedValues, boidProperties.matchVelocityWeight);
		PlayerPrefs.SetFloat("TargetWeight_" + numberOfSavedValues, boidProperties.targetWeight);

		savedProperties.Add(boidProperties);
		numberOfSavedValues++;
		PlayerPrefs.SetInt("NumberOfSavedValues", numberOfSavedValues);
		PlayerPrefs.Save();
	}

	[ContextMenu("Clear Player Prefs")]
	private void ClearPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
	}
}