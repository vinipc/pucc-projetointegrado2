﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour 
{
	new protected Rigidbody2D rigidbody;

	public bool rotates = false;
	private Animator[] animators;
	public float maxVelocity;
	public float maxMoveForce;

	[HideInInspector]
	public Vector2 direction;
	[HideInInspector]
	public float currentVelocity;
	[HideInInspector]
	public float currentMoveForce;

	protected virtual void Awake()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}

	protected virtual void Update()
	{
		currentVelocity = rigidbody.velocity.magnitude;
		currentMoveForce = Mathf.Lerp(maxMoveForce, 0f, Mathf.InverseLerp(0f, maxVelocity, currentVelocity));

		animators = GetComponentsInChildren<Animator>(true);

		if (rotates)
		{
			UpdateRotation();
		}
		else if (animators[0] != null)
		{
			float angle = Mathf.Atan2(rigidbody.velocity.y, rigidbody.velocity.x) * Mathf.Rad2Deg;
			bool isMoving = currentVelocity > 0f;

			for (int i = 0; i < animators.Length; i++)
			{
				animators[i].SetFloat("angle", angle);
				animators[i].enabled = isMoving;
			}
		}
	}

	protected virtual void UpdateRotation()
	{
		if (rigidbody.velocity.sqrMagnitude > 0f)
		{
			direction = rigidbody.velocity.normalized;
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler(0f, 0f, angle - 90f);
		}
	}

	public void OnDrawGizmos()
	{
		if (rigidbody != null)
		{
			Gizmos.DrawLine(transform.position, transform.position + (Vector3) rigidbody.velocity);
		}
	}
}