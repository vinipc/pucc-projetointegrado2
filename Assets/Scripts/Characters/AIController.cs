﻿using UnityEngine;
using System.Collections;

public class AIController : Controller 
{
	public bool watchesEnemy = false;
	public bool watchesPlayer = false;

	public Transform currentTarget;

	protected override void Awake()
	{
		base.Awake();
	}

	protected virtual void FixedUpdate()
	{
		FollowTarget();
	}

	private void FollowTarget()
	{
		Vector3 targetPosition;
		if (currentTarget != null)
		{
			targetPosition = currentTarget.position;
		}
		else
		{
			targetPosition = Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 0.5f));
			targetPosition.z = 0f;
		}
		
		Vector3 moveDirection = (targetPosition - transform.position).normalized;
		rigidbody.AddForce(moveDirection * currentMoveForce);
	}

	public void ObjectEnteredWatchArea(Transform newObject)
	{
		if (watchesEnemy && newObject.GetComponent<Enemy>() || watchesPlayer && newObject.GetComponent<Player>())
		{
			currentTarget = newObject;
		}
	}
}