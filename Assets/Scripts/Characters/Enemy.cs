﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Enemy : Character 
{
	public Collider2D frontAttackCollider;
	public GameObject droppedPowerPrefab;

	protected override void Awake()
	{
		base.Awake();

		Collider2D[] childColliders = GetComponentsInChildren<Collider2D>();
		SpriteRenderer renderer = GetComponentInChildren<SpriteRenderer>();
		SpriteRenderer[] childRenderers = GetComponentsInChildren<SpriteRenderer>();

		controller.enabled = false;

		collider.enabled = false;
		for (int i = 0; i < childColliders.Length; i++)
		{
			childColliders[i].enabled = false;
		}

		renderer.color = new Color(1, 1, 1, 0);
		renderer.DOFade(1f, 1f).OnComplete(FadeComplete);

		for (int i = 0; i < childRenderers.Length; i++)
		{
			childRenderers[i].color = new Color(1, 1, 1, 0);
			childRenderers[i].DOFade(1f, 1f);
		}
	}

	public void FadeComplete()
	{
		Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
		SpriteRenderer[] childRenderers = GetComponentsInChildren<SpriteRenderer>();

		controller.enabled = true;
		collider.enabled = true;
		for (int i = 0; i < colliders.Length; i++)
		{
			colliders[i].enabled = true;
		}

		for (int i = 0; i < childRenderers.Length; i++)
		{
			childRenderers[i].color = Color.white;
		}
	}

	public virtual void OffensiveTriggerEntered(Collider2D otherColl, Collider2D defensiveTrigger) { }

	public virtual void OffensiveColliderHit(Collision2D collision, Collider2D offensiveCollider) 
	{
		if (offensiveCollider == frontAttackCollider)
		{
			Character hitCharacter = collision.gameObject.GetComponent<Character>();
			if (hitCharacter)
			{
				hitCharacter.HitByBasicPower(gameObject);
			}
		}
	}

	public virtual void HitByPlayerSpike(Spike spike)
	{
		DecreaseLives(5);
		ActivateTemporaryImmunity(1f);
	}

	public virtual void HitByPlayerShell(Shell shell)
	{
		DecreaseLives(1);
		ActivateTemporaryImmunity(1f);
	}

	public virtual void HitByPlayerFreeze()
	{
		DecreaseLives(1);
		ActivateTemporaryImmunity(1f);
	}

	protected override void Die()
	{
		AudioMaster.MonsterDeath();
		Destroy(gameObject);
		if (Random.value <= 0.75f)
		{
			Instantiate(droppedPowerPrefab, transform.position, Quaternion.identity);
		}
	}
}