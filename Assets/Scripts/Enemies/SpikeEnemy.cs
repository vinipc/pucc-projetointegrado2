﻿using UnityEngine;
using System.Collections;

public class SpikeEnemy : Enemy 
{
	public Collider2D spikeCollider;

	public override void OffensiveColliderHit(Collision2D collision, Collider2D offensiveCollider)
	{
		if (offensiveCollider == spikeCollider)
		{
			Character hitCharacter = collision.gameObject.GetComponent<Character>();
			if (hitCharacter)
			{
				hitCharacter.HitBySpikedEnemy(gameObject);
			}
		}
	}
}