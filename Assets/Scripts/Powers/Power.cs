﻿using UnityEngine;
using System.Collections;

public abstract class Power : MonoBehaviour 
{
	public bool isAttached { get { return transform.parent != null; } }
	protected bool isLaunched = false;

	public float lifetime;

	public abstract void ActivatePower(Player player);

	private void Update()
	{
		if (!isAttached)
		{
			lifetime -= Time.deltaTime;
		}

		if (lifetime <= 0f)
		{
			Destroy(gameObject);
		}
	}

	public virtual void OnTriggerEnter2D(Collider2D otherCollider)
	{
		Player hitCharacter = otherCollider.GetComponent<Player>();
		if (hitCharacter)
		{
			hitCharacter.PickUpPower(this);
		}
	}

	public virtual void Unattach(bool throwAway = false)
	{	
		Destroy(gameObject);
	}

	public virtual void AttachToCharacter(Transform newOwner)
	{
		transform.SetParent(newOwner);
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;

		newOwner.GetComponent<Player>().currentPower = this;
		gameObject.SetActive(false);
	}

	public virtual void DestroyPower()
	{
		this.Unattach();
		Destroy(gameObject);
	}
}