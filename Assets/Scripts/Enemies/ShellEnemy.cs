﻿using UnityEngine;
using System.Collections;

public class ShellEnemy : Enemy
{
	public Collider2D shellCollider;

	public override void OffensiveColliderHit(Collision2D collision, Collider2D offensiveCollider)
	{
		base.OffensiveColliderHit(collision, offensiveCollider);

		if (offensiveCollider == shellCollider)
		{
			Character hitCharacter = collision.gameObject.GetComponent<Character>();
			if (hitCharacter)
			{
				hitCharacter.HitByShelledEnemy(gameObject);
			}
		}
	}

	public override void HitBySpikedEnemy(GameObject enemy)
	{
		Vector2 direction = (enemy.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 3f, ForceMode2D.Impulse);
		DecreaseLives(1);
	}
}
