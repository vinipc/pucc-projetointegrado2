﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using DG.Tweening;

public class MenuController : MonoBehaviour 
{
	public Image blackscreen;

	private void Awake()
	{
		Time.timeScale = 1f;

		blackscreen.DOFade(0f, 0.5f).SetUpdate(true);
		for (int i = 0; i < blackscreen.transform.childCount; i++)
		{
			blackscreen.transform.GetChild(i).GetComponent<Image>().DOFade(0f, 0.5f).SetUpdate(true);
		}
	}

	public void StartGameButtonPressed(int numberOfPlayers)
	{
		PlayerPrefs.SetInt("numberOfPlayers", numberOfPlayers);
		PlayerPrefs.Save();

		blackscreen.DOFade(1f, 0.5f).OnComplete(StartGame).SetUpdate(true);
		for (int i = 0; i < blackscreen.transform.childCount; i++)
		{
			blackscreen.transform.GetChild(i).GetComponent<Image>().DOFade(1f, 0.5f).SetUpdate(true);
		}
	}

	public void StartGame()
	{
		SceneManager.LoadScene("MainScene");
	}

	public void ExitButtonPressed()
	{
		blackscreen.DOFade(1f, 0.5f).OnComplete(ExitGame);
	}

	private void ExitGame()
	{
		Application.Quit();
	}
}
