﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour 
{
	public List<GameObject> lives = new List<GameObject>();

	protected Vector3 lastPosition;
	new protected Rigidbody2D rigidbody;
	new protected Collider2D collider;
	protected Controller controller;

	public bool isSlowed = false;
	public bool isImmune = false;
	public bool isTurboed = false;

	public float defaultMaxVelocity;
	public float defaultMaxMoveForce;

	protected virtual void Awake()
	{
		collider = GetComponent<Collider2D>();
		rigidbody = GetComponent<Rigidbody2D>();
		controller = GetComponent<Controller>();
	}

	protected virtual void Update()
	{
		if (lives.Count <= 0)
		{
			UpdateLivesDisplay();
			Die();
		}

		UpdateControllerValues();
	}

	protected virtual void LateUpdate()
	{
		UpdateLivesDisplay();
	}

	[ContextMenu("Die")]
	protected virtual void Die()
	{
		Destroy(gameObject);
	}

	protected virtual void DecreaseLives(int amount)
	{
		if (isImmune)
			return;

		for (int i = 0; i < amount; i++)
		{
			if (lives.Count > 0)
			{
				Destroy(lives[0]);
				lives.RemoveAt(0);
			}
		}

		UpdateLivesDisplay();
	}

	protected virtual void UpdateLivesDisplay()
	{
		for (int i = 0; i < lives.Count; i++)
		{
			Vector3 position = transform.position;
			position.x = transform.position.x + 0.5f * (i - 1);
			position.y = transform.position.y + 1f;
			lives[i].transform.position = position;
		}
	}

	protected virtual void UpdateControllerValues()
	{
		float newMaxVelocity = defaultMaxVelocity;
		newMaxVelocity *= isSlowed ? 0.6f : 1f;
		newMaxVelocity *= isTurboed ? 2f : 1f;

		float newMaxMoveForce = defaultMaxMoveForce;
		newMaxMoveForce *= isSlowed ? 0.6f : 1f;
		newMaxMoveForce *= isTurboed ? 1.6f : 1f;

		controller.maxVelocity = newMaxVelocity;
		controller.maxMoveForce = newMaxMoveForce;
	}

	public virtual void HitByBasicPower(GameObject enemy)
	{
		Vector2 direction = (enemy.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 3f, ForceMode2D.Impulse);
		DecreaseLives(1);
		ActivateTemporaryImmunity(1f);
	}

	public virtual void HitBySpikedEnemy(GameObject enemy)
	{
		Vector2 direction = (enemy.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 3f, ForceMode2D.Impulse);
		DecreaseLives(2);
		ActivateTemporaryImmunity(1f);
	}

	public virtual void HitByShelledEnemy(GameObject enemy)
	{
		Vector2 direction = (enemy.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 3f, ForceMode2D.Impulse);
		ActivateTemporarySlow(0.3f);
	}

	public virtual void HitByFrozenEnemy(GameObject enemy)
	{
		Vector2 direction = (enemy.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 3f, ForceMode2D.Impulse);
		ActivateTemporarySlow(1f);
	}

	public virtual void HitByTurbo(GameObject hitObject = null)
	{
		DecreaseLives(1);
		Vector2 direction = (hitObject.transform.position - transform.position).normalized;
		rigidbody.AddForce(-direction * 3f, ForceMode2D.Impulse);
	}

	protected void ActivateTemporaryImmunity(float time)
	{
		if (!isImmune)
		{
			StartCoroutine(ImmunityCoroutine(time));
		}
	}

	protected IEnumerator ImmunityCoroutine(float time)
	{
		isImmune = true;
		yield return new WaitForSeconds(time);
		isImmune = false;
	}

	protected void ActivateTemporarySlow(float time)
	{
		StartCoroutine(SlowCoroutine(time));
	}

	protected IEnumerator SlowCoroutine(float time)
	{
		isSlowed = true;
		yield return new WaitForSeconds(time);
		isSlowed = false;
	}

	protected void ActivateTemporaryTurbo(float time)
	{
		StartCoroutine(TurboCoroutine(time));
	}

	protected IEnumerator TurboCoroutine(float time)
	{
		isTurboed = true;
		yield return new WaitForSeconds(time);
		isTurboed = false;
	}

	private void OnDrawGizmos()
	{
		if (isSlowed)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(transform.position + Vector3.up * 1.3f, 0.15f);
		}
	}
}