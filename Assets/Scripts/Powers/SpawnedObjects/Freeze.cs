﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Freeze : MonoBehaviour 
{
	public List<Enemy> affectedEnemies;

	public float damageCountdown;
	private float currentCountdown = 0f;

	private void Awake()
	{
		affectedEnemies = new List<Enemy>();
		StartCoroutine(LifetimeCoroutine());
	}

	private void Update()
	{
		currentCountdown -= Time.deltaTime;
		if (currentCountdown <= 0f)
		{
			FreezeAffectedEnemies();
			currentCountdown = damageCountdown;
		}
	}

	public void OnTriggerEnter2D(Collider2D otherColl)
	{
		Enemy hitEnemy = otherColl.GetComponent<Enemy>();
		if (hitEnemy)
		{
			affectedEnemies.Add(hitEnemy);
			hitEnemy.isSlowed = true;
		}
	}
	
	public void OnTriggerExit2D(Collider2D otherColl)
	{
		Enemy hitEnemy = otherColl.GetComponent<Enemy>();
		if (hitEnemy)
		{
			affectedEnemies.Remove(hitEnemy);
			hitEnemy.isSlowed = false;
		}
	}

	private void FreezeAffectedEnemies()
	{
		for (int i = 0; i < affectedEnemies.Count; i++)
		{
			if (affectedEnemies[i] != null)
			{
				affectedEnemies[i].HitByPlayerFreeze();
			}
		}
	}

	private IEnumerator LifetimeCoroutine()
	{
		yield return new WaitForSeconds(5f);
		GetComponentInParent<Player>().OnPowerDestroyed();
		Destroy(gameObject);
	}

}