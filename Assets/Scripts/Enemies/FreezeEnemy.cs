﻿using UnityEngine;
using System.Collections;

public class FreezeEnemy : Enemy 
{
	public Collider2D freezeArea;

	public override void OffensiveTriggerEntered(Collider2D otherCollider, Collider2D offensiveCollider)
	{
		if (offensiveCollider == freezeArea)
		{
			Character affectedCharacter = otherCollider.GetComponent<Character>();
			if (affectedCharacter != null)
			{
				affectedCharacter.HitByFrozenEnemy(gameObject);
			}
		}
	}
}