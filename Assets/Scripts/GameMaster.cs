﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

[System.Serializable]
public struct PlayerDisplayPair
{
	public Player player;
	public GameObject display;
	public GameObject instructions;
}

public class GameMaster : MonoBehaviour 
{
	public List<PlayerDisplayPair> playerDisplayPairs = new List<PlayerDisplayPair>();
	public GameObject gameStartScreen;
	public GameObject gameOverScreen;
	public GameObject pauseScreen;
	public Image blackscreen;

	public EnemySpawner enemySpawner;
	[Range(1, 4)] public int numberOfPlayers;

	private static GameMaster _instance;
	public static GameMaster instance
	{
		get
		{
			if (_instance == null)
				_instance = GameObject.Find("GameMaster").GetComponent<GameMaster>();

			return _instance;
		}
	}

	public static bool isPaused = false;

	private void Awake()
	{
		numberOfPlayers = PlayerPrefs.GetInt("numberOfPlayers");
		Time.timeScale = 1f;

		blackscreen.DOFade(0f, 1f);
		for (int i = 0; i < blackscreen.transform.childCount; i++)
		{
			blackscreen.transform.GetChild(i).GetComponent<Image>().DOFade(0f, 1f).SetUpdate(true);
		}

		if (numberOfPlayers < 4)
		{
			Destroy(playerDisplayPairs[3].player.gameObject);
			Destroy(playerDisplayPairs[3].display);
			Destroy(playerDisplayPairs[3].instructions);
			playerDisplayPairs.RemoveAt(3);
		}

		if (numberOfPlayers < 3)
		{
			Destroy(playerDisplayPairs[2].player.gameObject);
			Destroy(playerDisplayPairs[2].display);
			Destroy(playerDisplayPairs[2].instructions);
			playerDisplayPairs.RemoveAt(2);
		}

		if (numberOfPlayers < 2)
		{
			Destroy(playerDisplayPairs[1].player.gameObject);
			Destroy(playerDisplayPairs[1].display);
			Destroy(playerDisplayPairs[1].instructions);
			playerDisplayPairs.RemoveAt(1);
		}

		Chronometer.count = false;
		enemySpawner.enabled = false;
	}

	private void Update () 
	{
		if (Input.anyKeyDown && !enemySpawner.enabled && blackscreen.color.a == 0f)
		{
			Chronometer.count = true;
			enemySpawner.enabled = true;
			gameStartScreen.SetActive(false);
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			TogglePause();
		}

		if (Input.GetKeyDown(KeyCode.R))
		{
			RestartButtonPressed();
		}
	}

	private void TogglePause()
	{
		if (isPaused)
		{
			isPaused = false;
			pauseScreen.SetActive(false);
			Time.timeScale = 1f;
		}
		else
		{
			isPaused = true;
			pauseScreen.SetActive(true);
			pauseScreen.transform.FindChild("Chronometer/Time").GetComponent<Text>().text = Chronometer.time.ToString("0.0");
			Time.timeScale = 0f;
		}
	}

	public static void RemovePlayer(Player player)
	{
		PlayerDisplayPair playerDisplayPair = instance.playerDisplayPairs[0];
		for (int i = 0; i < instance.playerDisplayPairs.Count; i++)
		{
			if (instance.playerDisplayPairs[i].player == player)
				playerDisplayPair = instance.playerDisplayPairs[i];
		}
		instance.playerDisplayPairs.Remove(playerDisplayPair);

		if (instance.playerDisplayPairs.Count == 0)
		{
			instance.GameOver();
		}
	}

	public static Player GetRandomPlayer()
	{
		return instance.playerDisplayPairs[Random.Range(0, instance.playerDisplayPairs.Count)].player;
	}

	private void GameOver()
	{
		gameOverScreen.SetActive(true);
		Chronometer.count = false;

		gameOverScreen.transform.FindChild("Chronometer/Time").GetComponent<Text>().text = Chronometer.time.ToString("0.0");
	}

	public void MenuButtonPressed()
	{
		blackscreen.DOFade(1f, 0.5f).OnComplete(LoadMainMenu).SetUpdate(true);
		for (int i = 0; i < blackscreen.transform.childCount; i++)
		{
			blackscreen.transform.GetChild(i).GetComponent<Image>().DOFade(1f, 0.5f).SetUpdate(true);
		}
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}

	public void RestartButtonPressed()
	{
		blackscreen.DOFade(1f, 0.5f).OnComplete(ReloadMainScene).SetUpdate(true);
		for (int i = 0; i < blackscreen.transform.childCount; i++)
		{
			blackscreen.transform.GetChild(i).GetComponent<Image>().DOFade(1f, 0.5f).SetUpdate(true);
		}
	}

	public void ReloadMainScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}