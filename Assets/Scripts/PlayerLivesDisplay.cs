﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerLivesDisplay : MonoBehaviour 
{
	public GameObject[] sprites;
	public Image display;

	public void UpdateDisplay(int numberOfLives)
	{
		if (numberOfLives == 0)
		{
			display.gameObject.SetActive(false);
			return;
		}

		for (int i = 0; i < sprites.Length; i++)
		{
			sprites[i].SetActive(i < numberOfLives);
		}
	}
}