﻿using UnityEngine;
using System.Collections;

public class Shell : MonoBehaviour 
{
	new public Rigidbody2D rigidbody;
	public float force;
	public float lifetime = 5f;

	public void OnTriggerEnter2D(Collider2D otherColl)
	{
		Enemy hitEnemy = otherColl.GetComponent<Enemy>();

		if (hitEnemy)
		{
			hitEnemy.HitByPlayerShell(this);
		}
	}

	public void ThrowShell(Vector3 direction)
	{
		StartCoroutine(ThrowShellCoroutine(direction));
	}

	private IEnumerator ThrowShellCoroutine(Vector3 direction)
	{
		rigidbody.AddForce(direction * force, ForceMode2D.Impulse);

		yield return new WaitForSeconds(lifetime);

		Destroy(gameObject);
	}
}
