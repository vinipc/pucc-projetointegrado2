﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AudioMaster : MonoBehaviour 
{
	private static AudioMaster _instance;
	public static AudioMaster instance
	{
		get
		{
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<AudioMaster>();
			return _instance;
		}
	}

	new private AudioSource audio;
	public static bool sfxOn;

	public AudioSource playerHurt;
	public AudioSource monsterDeath;

	private void Awake()
	{
		audio = GetComponent<AudioSource>();
		sfxOn = true;
	}		

	public void ToggleMusic(bool isMuted)
	{
		DOTween.Kill("MusicToggle");
		audio.DOFade((isMuted) ? 0f : 1f, 0.3f).SetUpdate(true).SetId("MusicToggle");
	}

	public void ToggleSFX(bool isOn)
	{
		sfxOn = isOn;
	}

	public static void PlayerHurt()
	{
		if (sfxOn)
		{
			instance.playerHurt.PlayOneShot(instance.playerHurt.clip);
		}
	}

	public static void MonsterDeath()
	{
		if (sfxOn)
		{
			instance.monsterDeath.PlayOneShot(instance.monsterDeath.clip);
		}
	}
}
