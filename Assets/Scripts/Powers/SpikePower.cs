﻿using UnityEngine;
using System.Collections;

public class SpikePower : Power 
{
	public GameObject spikePrefab;

	public override void ActivatePower(Player player)
	{
		Vector3 direction = (Vector3) player.GetComponent<Controller>().direction;
		
		Vector3 spawnPosition = player.transform.position + direction * 1.2f;
		GameObject spawnedSpike = (GameObject) Instantiate(spikePrefab, spawnPosition, player.transform.rotation);
		
		spawnedSpike.GetComponent<Spike>().ThrowSpike(direction);
		player.OnPowerDestroyed();
		DestroyPower();
	}
}